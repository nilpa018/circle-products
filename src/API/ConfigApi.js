// For good practice we will put this file in an .env file

export const API_URI = "https://fakestoreapi.com/";
export const LIMIT = "products?limit=";
export const PRODUCT = "products/";
