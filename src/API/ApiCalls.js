import { API_URI, LIMIT, PRODUCT } from "./ConfigApi";

/**
 * This API call returns a promise containing the list of products according to the limit number entered
 * @param {Number} limitNumber The number of results expected in the api call
 * @returns {Promise}
 */

export async function getAllApiProducts(limitNumber) {
  const response = await fetch(`${API_URI}${LIMIT}${limitNumber}`)
    .then((res) => res.json())
    .catch((error) => {
      console.error(
        "error trying to fetch products on the fakeStoreApi : " + error
      );
    });

  return response;
}

/**
 * This api call allows you to update the price of the current product in the api (dummy) and save it locally
 * @param {Object} productObj The object containing the current product list
 * @param {Number} newPrice The new price of the product to update
 * @returns {Promise}
 */

export async function updateApiProductPrice(productObj, newPrice) {
  let products = JSON.parse(localStorage.getItem("products"));
  let indexOfProduct = products.findIndex(
    (product) => product.id == productObj.id
  );
  let newProduct = { ...productObj };
  newProduct.price = parseFloat(newPrice);

  const response = await fetch(`${API_URI}${PRODUCT}${productObj.id}`, {
    method: "PUT",
    body: JSON.stringify(newProduct),
  })
    .then((res) => res.json())
    .then(() => {
      products[indexOfProduct] = newProduct;
      localStorage.setItem("products", JSON.stringify(products));
    })
    .catch((error) => {
      console.error(
        "error trying to fetch products on the fakeStoreApi : " + error
      );
    });

  return response;
}
