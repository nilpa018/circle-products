/**
 * Function that return the correct class according to the category name
 * @param {String} category the name of the category
 * @returns {String} the class corresponding to the category
 */
export const setClassFromCategoryName = (category) => {
  switch (category) {
    case "men's clothing":
      return "main__category--mens-clothing";
    case "jewelery": {
      return "main__category--jewelery";
    }
    default:
      return "main__category--unknown";
  }
};

/**
 * Calculate the price with VAT from the original price
 * @param {number} price the original price
 * @param {Number} vat default value is set to 20
 * @return {Number} The price including VAT
 */
export const priceWithVAT = (price, vat = 20) => {
  return parseFloat(price + (price * vat) / 100).toFixed(2);
};

/**
 * Sort an array of Objects by his property "category"
 * ex: sortProductsByCategory([{id:1, name:"Lucie"}, {id:2, name:"Jhon"}],"name","asc")  return [{id:2, name:"Jhon"},{id:1, name:"Lucie"}]
 * @param {Array} products The array of object you want to sort
 * @param {String} sortType Can only be "asc" or "desc"
 * @return {Array} return the sorted Array
 */
export const sortProductsByCategory = (products, sortType) => {
  if (sortType == "asc") {
    products.sort((a, b) => {
      return b.category.localeCompare(a.category);
    });
  } else {
    products.sort((a, b) => {
      return a.category.localeCompare(b.category);
    });
  }
  return products;
};
