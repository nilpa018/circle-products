import { useState } from "react";
import { useParams } from "react-router-dom";
import Layout from "../components/Layout";
import { setClassFromCategoryName, priceWithVAT } from "../utils/functions";
import { updateApiProductPrice } from "../API/ApiCalls";
import Title from "../components/Title";

const ProductDetails = () => {
  const params = useParams();
  const product = JSON.parse(localStorage.getItem("products")).filter(
    (product) => product.id == params.id
  )[0];
  const [alert, setAlert] = useState({});
  const [updatePrice, setUpdatePrice] = useState(product.price);
  const isUpdatable = updatePrice != product.price;
  const updateProductPrice = (e) => {
    setUpdatePrice(e.target.value);
  };

  const updateCurrentPriceProduct = (product, updatePrice) => {
    updateApiProductPrice(product, updatePrice)
      .then(() => {
        setAlert({
          type: "success",
          message: "product has been successfully modified",
        });
        setTimeout(() => {
          setAlert({});
        }, 3000);
      })
      .catch((error) => {
        setAlert({
          message: "error updating the product : " + error.message,
          type: "danger",
        });
      });
  };

  return (
    <>
      <Layout>
        <div>
          <div
            className={
              (Object.keys(alert).length !== 0 ? "main__alert--active " : "") +
              "main__alert main__alert--" +
              alert.type
            }
          >
            {alert.message}
          </div>
          <div className="main__product-title">
            <Title hasBackIcon={true} title={product.title} />
          </div>
          <div
            className={
              "main__product-detail main__product-detail--" +
              localStorage.getItem("device")
            }
          >
            <div className="main__product-detail-image">
              <img src={product.image} alt={product.title} />
            </div>
            <div className="main__product-detail-description-price">
              <div className="main__product-detail-description">
                <h2>Description</h2>
                <p>{product.description}</p>
              </div>
              <div className="main__product-detail-price">
                <h2>Price</h2>
                <div>
                  <div className="main__product-detail-price-input-group">
                    <input
                      type="text"
                      defaultValue={updatePrice}
                      onChange={(e) => {
                        updateProductPrice(e);
                      }}
                      pattern="[0-9]+(\.?[0-9]{0,2})"
                    />
                    <span className="currency">€</span>
                  </div>
                  <div>
                    <span>
                      <b>Price</b> (including VAT):{" "}
                      {priceWithVAT(product.price)}€
                    </span>
                  </div>
                </div>
                <button
                  onClick={() =>
                    updateCurrentPriceProduct(product, updatePrice)
                  }
                  disabled={!isUpdatable}
                >
                  Update product
                </button>
              </div>
            </div>
            <div className="main__product-detail-category">
              <h2>Category</h2>
              <span
                className={
                  "main__category " + setClassFromCategoryName(product.category)
                }
              >
                {product.category}
              </span>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default ProductDetails;
