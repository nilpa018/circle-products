import Layout from "../components/Layout";

const Logout = () => {
  return (
    <>
      <Layout>
        <h1>Logout</h1>
      </Layout>
    </>
  );
};

export default Logout;
