import Layout from "../components/Layout";

const ErrorNotFoundPage = () => {
  return (
    <>
      <Layout>
        <h1>404</h1>
      </Layout>
    </>
  );
};

export default ErrorNotFoundPage;
