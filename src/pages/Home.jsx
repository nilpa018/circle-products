import { useEffect, useState } from "react";
import Layout from "../components/Layout";
import ProductsTable from "../components/ProductsTable";
import { getAllApiProducts } from "../API/ApiCalls";

const Home = () => {
  const limitNumber = 7;
  const [products, setProducts] = useState(
    JSON.parse(localStorage.getItem("products")) || null
  );

  useEffect(() => {
    if (!products) {
      getAllApiProducts(limitNumber).then((products) => {
        localStorage.setItem("products", JSON.stringify(products));
        setProducts(products);
      });
    }
  }, [products]);

  return (
    <>
      <Layout>
        <h1>Home</h1>
        {!products ? <p>Loading ...</p> : <ProductsTable products={products} />}
      </Layout>
    </>
  );
};

export default Home;
