import "../../assets/style/style.less";
import "../../assets/style/mobile-style.less";
import SideBar from "./Sidebar";
import MenuHamburger from "./menuMobile/MenuHamburger";

const Nav = ({ screenSize }) => {
  return (
    <>
      {screenSize <= 440 || screenSize <= 1025 ? (
        <MenuHamburger />
      ) : (
        <aside className="sidebar">
          <SideBar />
        </aside>
      )}
    </>
  );
};

export default Nav;
