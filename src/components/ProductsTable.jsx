import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ChevronDown from "../components/icons/chevronDown";
import ChevronUp from "../components/icons/chevronUp";
import {
  setClassFromCategoryName,
  priceWithVAT,
  sortProductsByCategory,
} from "../utils/functions";

function ProductsTable({ products }) {
  const navigate = useNavigate();
  const [sortByCategory, setSortByCategory] = useState("desc");

  /**
   * Change the sort order when click on category on the table
   */
  const handleClickSortByCategory = () => {
    if (sortByCategory == "asc") {
      setSortByCategory("desc");
    } else {
      setSortByCategory("asc");
    }
    sortProductsByCategory(products, sortByCategory);
  };

  return (
    <table
      className={
        "main__table-display-data main__table-display-data--" +
        localStorage.getItem("device")
      }
    >
      <thead>
        <tr>
          <th>Product name</th>
          <th
            className="main__table-display-data-thead-category"
            onClick={handleClickSortByCategory}
          >
            Category{" "}
            {sortByCategory == "desc" ? <ChevronDown /> : <ChevronUp />}
          </th>
          <th>Price</th>
          <th>
            Price <span className="main__info-thead">(including VAT)</span>
          </th>
        </tr>
      </thead>
      <tbody>
        {products &&
          products.map((product) => {
            return (
              <tr
                key={product.id}
                onClick={() => navigate("./products/" + product.id)}
              >
                <td>{product.title}</td>
                <td>
                  <span
                    className={
                      setClassFromCategoryName(product.category) +
                      " main__category"
                    }
                  >
                    {product.category}
                  </span>
                </td>
                <td>{product.price}€</td>
                <td>{priceWithVAT(product.price)}€</td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
}

export default ProductsTable;
