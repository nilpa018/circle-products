import { useEffect, useState } from "react";
import Nav from "../components/nav/Nav";

function Layout({ children }) {
  const device = localStorage.getItem("device");
  const [screenSize, setScreenSize] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setScreenSize(window.innerWidth);
      if (screenSize <= 440 || screenSize <= 1025) {
        localStorage.setItem("device", "mobile");
      } else {
        localStorage.setItem("device", "desktop");
      }
    };

    window.addEventListener("resize", handleResize);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [screenSize]);

  return (
    <>
      <div style={{ display: "flex" }} className={device}>
        <Nav screenSize={screenSize} />
        <main className="container">{children}</main>
      </div>
    </>
  );
}

export default Layout;
