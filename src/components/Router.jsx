import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home";
import ProductsManagement from "../pages/ProductsManagement";
import EmployeesManagement from "../pages/EmployeesManagement";
import Logout from "../pages/Logout";
import ErrorNotFoundPage from "../pages/ErrorNotFoundPage";
import ProductDetails from "../pages/ProductDetails";

const Router = () => {
  return (
    <>
      <Routes>
        <Route index path="/" element={<Home />} />
        <Route path="/employeesManagement" element={<EmployeesManagement />} />
        <Route path="/productsManagement" element={<ProductsManagement />} />
        <Route path="/products/:id" element={<ProductDetails />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="*" element={<ErrorNotFoundPage />} />
      </Routes>
    </>
  );
};

export default Router;
